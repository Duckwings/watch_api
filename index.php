<!doctype html>
<html>
<head>
  <meta charset="utf-8">
   <title>POPWatch</title>
   <link rel="stylesheet" type="text/css" href="style.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
   integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>

<body>
  <?php
  include("config.php");
  ?>

<div class='container-fluid'>
  <div class="row">
    <nav class="navbar navbar-inverse navbar-static-top" id='nav'>
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed menu-btn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class='nav navbar-nav col-md-12'>
            <li class="col-md-2 col-md-offset-1">
              <a href='index.php' title='' class="text-uppercase text-center">home</a>
            </li>
            <li class="dropdown col-md-2">
              <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
              role="button" aria-haspopup="true" aria-expanded="false">watchs<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="text-uppercase text-center"><a href="#">category 1</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="#">category 2</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="#">category 3</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="watch.php">see all</a></li>
              </ul>
            </a>
          </li>
          <li class="col-md-2">
            <a href='' title='' class="text-uppercase text-center" data-toggle="modal" data-target="#random">random</a>
          </li>
          <li class="dropdown col-md-2">
            <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
            role="button" aria-haspopup="true" aria-expanded="false">users<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="text-uppercase text-center"><a href="#">alan turing</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="#">ada lovelace</a></li>
            </ul>
          </a>
        </li>
        <li class="col-md-2">
          <a href='' title='' class="text-uppercase text-center" data-toggle="modal" data-target="#account">log in</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>


<div class="modal fade" id="account" tabindex="-1" role="dialog" aria-labelledby="Connexion">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="Connexion">Connexion</h4>
      </div>
      <div class="modal-body">
        <form action="login.php" method="get">
          <input name='username' type="text" placeholder="Username">
          <label for='username'></label>
          <input name='password' type="password" placeholder="Password">
          <label for='password'></label>
      </div>
      <div class="modal-footer">
        <input class="btn btn-primary" type="submit" value='Log in'>
        <h4 class="text-center"> Don't have an account yet ? </h4>
        <a href='sign.php' title=''><button type="button" class="btn btn-default sign">Sign in</button></a>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="random" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Spin the watch wheel</h4>
      </div>
      <div class="modal-body">
          <label for="rand1" class="col-md-2 col-md-offset-1">
            <input type="checkbox" id="rand1" value="1" checked> Amélie
          </label>
          <label for="rand2" class="col-md-2">
            <input type="checkbox" id="rand2" value="2" checked> Aurélie
          </label>
          <label for="rand3" class="col-md-2">
            <input type="checkbox" id="rand3" value="3" checked> Bastien
          </label>
          <label for="rand4" class="col-md-2">
            <input type="checkbox" id="rand4" value="4" checked> Christopher
          </label>
          <label for="rand5" class="col-md-2">
            <input type="checkbox" id="rand5" value="5" checked> Camille
          </label>
          <label for="rand6" class="col-md-2 col-md-offset-1">
            <input type="checkbox" id="rand6" value="6" checked> Jérémy
          </label>
          <label for="rand7" class="col-md-2">
            <input type="checkbox" id="rand7" value="7" checked> Ingrid
          </label>
          <label for="rand8" class="col-md-2">
            <input type="checkbox" id="rand8" value="8" checked> Selim
          </label>
          <label for="rand9" class="col-md-2">
            <input type="checkbox" id="rand9" value="9" checked> Loïs
          </label>
          <label for="rand10" class="col-md-2">
            <input type="checkbox" id="rand10" value="10" checked> Jean-pierre
          </label>
          <label for="rand11" class="col-md-2 col-md-offset-1">
            <input type="checkbox" id="rand11" value="11" checked> Rémy
          </label>
          <label for="rand12" class="col-md-2">
            <input type="checkbox" id="rand12" value="12" checked> Jimmylan
          </label>
          <label for="rand13" class="col-md-2">
            <input type="checkbox" id="rand13" value="13" checked> Kilian
          </label>
          <label for="rand14" class="col-md-2">
            <input type="checkbox" id="rand14" value="14" checked> Léo
          </label>
          <label for="rand15" class="col-md-2">
            <input type="checkbox" id="rand15" value="15" checked> Marielle
          </label>
          <label for="rand16" class="col-md-2 col-md-offset-1">
            <input type="checkbox" id="rand16" value="16" checked> Maxime
          </label>
          <label for="rand17" class="col-md-2">
            <input type="checkbox" id="rand17" value="17" checked> Maxence
          </label>
          <label for="rand18" class="col-md-2">
            <input type="checkbox" id="rand18" value="18" checked> Pierre
          </label>
          <label for="rand19" class="col-md-2">
            <input type="checkbox" id="rand19" value="19" checked> Mathilde
          </label>
          <label for="rand20" class="col-md-2">
            <input type="checkbox" id="rand20" value="20" checked> Théo
          </label>
          <label for="rand21" class="col-md-2 col-md-offset-4">
            <input type="checkbox" id="rand21" value="21" checked> Victor
          </label>
          <label for="rand22" class="col-md-2">
            <input type="checkbox" id="rand22" value="22" checked> Younes
          </label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary spin">SPIN</button>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-10 col-md-offset-2">

  </div>
</div>


  <footer class="row">
  </footer>

</div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
