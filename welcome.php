<!doctype html>
<html>
<head>
  <meta charset="utf-8">
   <title>POPWatch</title>
   <link rel="stylesheet" type="text/css" href="style.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
   integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>

<body>
  <?php
  include("config.php");
  session_start();
  ?>

<div class='container-fluid'>
  <div class="row">
    <nav class="navbar navbar-inverse navbar-static-top" id='nav'>
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed menu-btn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class='nav navbar-nav col-md-12'>
            <li class="col-md-2 col-md-offset-1">
              <a href='index.php' title='' class="text-uppercase text-center">home</a>
            </li>
            <li class="dropdown col-md-2">
              <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
              role="button" aria-haspopup="true" aria-expanded="false">watchs<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="text-uppercase text-center"><a href="#">category 1</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="#">category 2</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="#">category 3</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="#">see all</a></li>
              </ul>
            </a>
          </li>
          <li class="col-md-2">
            <a href='' title='' class="text-uppercase text-center" data-toggle="modal" data-target="#random">random</a>
          </li>
          <li class="dropdown col-md-2">
            <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
            role="button" aria-haspopup="true" aria-expanded="false">users<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="text-uppercase text-center"><a href="#">alan turing</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="#">ada lovelace</a></li>
            </ul>
          </a>
        </li>
        <li class="dropdown col-md-2">
          <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
          role="button" aria-haspopup="true" aria-expanded="false">
          <?php
              echo  $_SESSION['username'];
           ?>
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="text-uppercase text-center"><a href="post.php">post</a></li>
            <li role="separator" class="divider"></li>
            <li class="text-uppercase text-center"><a href="logout.php">log out</a></li>
          </ul>
        </a>
      </li>
      </ul>
    </div>
  </div>
</nav>
</div>





</div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
