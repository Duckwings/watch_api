<!doctype html>
<html>
<head>
  <meta charset="utf-8">
   <title>POPWatch</title>
   <link rel="stylesheet" type="text/css" href="style.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
   integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>

<body>
  <?php
      ini_set("display_errors","true");
      $handle = mysqli_connect("localhost","root","1234","api_veille");
  ?>

<div class='container-fluid'>
  <div class="row">
    <nav class="navbar navbar-inverse navbar-static-top" id='nav'>
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed menu-btn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class='nav navbar-nav col-md-12'>
            <li class="col-md-2 col-md-offset-1">
              <a href='index.php' title='' class="text-uppercase text-center">home</a>
            </li>
            <li class="dropdown col-md-2">
              <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
              role="button" aria-haspopup="true" aria-expanded="false">watchs<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="text-uppercase text-center"><a href="#">category 1</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="#">category 2</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="#">category 3</a></li>
                <li role="separator" class="divider"></li>
                <li class="text-uppercase text-center"><a href="#">see all</a></li>
              </ul>
            </a>
          </li>
          <li class="col-md-2">
            <a href='' title='' class="text-uppercase text-center">random</a>
          </li>
          <li class="dropdown col-md-2">
            <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
            role="button" aria-haspopup="true" aria-expanded="false">users<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="text-uppercase text-center"><a href="#">alan turing</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="#">ada lovelace</a></li>
            </ul>
          </a>
        </li>
        <li class="col-md-2">
          <a href='' title='' class="text-uppercase text-center" data-toggle="modal" data-target="#account">log in</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>


<div class="modal fade" id="account" tabindex="-1" role="dialog" aria-labelledby="Connexion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="Connexion">Connexion</h4>
      </div>
      <div class="modal-body">
        <form action="login.php" method="get">
          <input name='username' type="text" placeholder="Username">
          <label for="username"></label>
          <input name='password' type="password" placeholder="Password">
          <label for="password"></label>
      </div>
      <div class="modal-footer">
        <input class="btn btn-primary" type="submit" value='Log in'>
        <h4 class="text-center"> Don't have an account yet ? </h4>
        <a href='sign.php' title=''><button type="button" class="btn btn-default sign">Sign in</button></a>
      </div>
      </form>
    </div>
  </div>
</div>

<form class="col-md-6 col-md-offset-3" action="account.php" method="get">
  <input name='name' type="text" placeholder="Name">
  <label for="name"></label>
  <input name='firstname' type="text" placeholder="First Name">
  <label for="firstname"></label>
  <select name='promo' class="form-control promo-log">
    <option selected="selected">ada-lovelace</option>
    <option >alan-turing</option>
  </select>
  <input name='mail' type="text" placeholder="Mail">
  <label for="mail"></label>
  <input name='username' type="text" placeholder="Username">
  <label for="username"></label>
  <input name='password' type="password" placeholder="Password">
  <label for="password"></label>
  <input name='cfpassword' type="password" placeholder="Confirm Password">
  <label for="cfpassword"></label>
  <input class="btn btn-primary" type="submit" value='Sign in'>
</form>

</div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
